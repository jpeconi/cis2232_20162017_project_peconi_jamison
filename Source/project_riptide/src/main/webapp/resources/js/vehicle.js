/* 
 * Javascript Referenced from Person.JS
 * Author: Jamison Peconi
 * Modified By: Patrick Fouchere
 * Modifications: Made it work with the vehicle page variables (Persons -> Vehicle)
 */

$(document).ready(function () {
    //disable delete on load
    $('#deleteVehicleLink').attr('disabled', true);
    //
// Call the method that handles the ajax request to the controller when a row is clicked
    $(".vehicleRow").click(function () {
        // Grab the value of the vehicle ID from the table
        var vin = $(this).find('td#vehicleIdRow').html();
        // Call the get vehicle details method and pass it the id
        getVehicleDetails(vin);
        //Enable delete button when vehicle selected
        $('#deleteVehicleLink').attr('disabled', false);
    });

// this function is the ajax request to the controller
// calls the getDetails() method and passes it the id of the vehicle clicked
// Returns a string which is converted to JSON and then to an object
    function getVehicleDetails(vin) {
        console.log($('#vehicleId').html());
        $.ajax({
            url: "/riptide/vehicle/getDetails",
            type: 'GET',
            dataType: 'json',
            contentType: "application/json",
            data: {vin: vin},
            success: function (data) {
                // convert the response to JSON
                var jsonVehicle = JSON.stringify(data);
                // Convert it to an object
                var vehicleObject = jQuery.parseJSON(jsonVehicle);

                //************** Debug ******************
                console.log(vehicleObject);


                // Debugging the ID grabbed from the AJAX call
                console.log("Id from the database ***** " + vehicleObject.vinNum);

                // fill the fields with the details of the object
                // the object will be the vehicle returned from the database  
                $('#destroyedBox').prop('checked', vehicleObject.destroyed);
                $('#licensePlate').val(vehicleObject.licensePlate);
                $('#tag').val(vehicleObject.tagRegistration);
                console.log("Tag:"+ vehicleObject.tagRegistration);
                $('#vinNum').val(vehicleObject.vinNum);
                $('#model').val(vehicleObject.model);
                $('#style').val(vehicleObject.style);
                $('#year').val(vehicleObject.year);
                $('#province').val(vehicleObject.province);
                $('#primeColor').val(vehicleObject.primeColor);
                console.log("Prime color:" + vehicleObject.primeColor);
                $('#secondColor').val(vehicleObject.secondColor);
                console.log("Second color:" + vehicleObject.secondColor);
                $('#commentBox').val(vehicleObject.comment);
                $('#regOwner').val(vehicleObject.regOwner);
                $('#towedBox').prop('checked', vehicleObject.isTowed);
                $('#storedBox').prop('checked', vehicleObject.stored);
                $('#releasedBox').prop('checked', vehicleObject.released);
                $('#stolenBox').prop('checked', vehicleObject.stolen);
                $('#recoveredBox').prop('checked', vehicleObject.recovered);
                $('#impoundedBox').prop('checked', vehicleObject.impounded);
                $('#leftAtSceneBox').prop('checked', vehicleObject.leftAtScene);
                $('#locationTowed').val(vehicleObject.locationTowed);
                $('#timeReported').val(vehicleObject.timeReported);
                $('#dateRecovered').val(vehicleObject.dateRecovered);
                $('#dateImpounded').val(vehicleObject.dateImpounded);
                
                //Adds vin to href to allow deletion
                $("#deleteVehicleLink").attr("href", "/riptide/vehicle/delete?id=" + vehicleObject.vinNum);
            }
        });
    }
    // Tie a method call to clear the form when the add button is clicked
    $('#add').click(function () {
        clearForm();
        $('#licensePlate').focus();
        //Disable the delet button when creating new vehicle
        $('#deleteVehicleLink').attr('disabled', true);
    });
    // Functionality to reset the form
    // When the add button is clicked all the form fields will be reset to their
    // defaults
    function clearForm() {
        $('#vehicleForm')[0].reset();
        $("#deleteVehicleLink").attr("href", "/riptide/vehicle/delete?id=");
        // Fix for the bug that wont reset hidden form fields
        $('#vinNum').val('');
    }

    // This functionality here will disable the button
    // Loop through each input in the form
    $('form').each(function () {
        // Serialize the data and store as "searialized"
        $(this).data('serialized', $(this).serialize());
        // Using the .on detect a change to any of the input fields
    }).on('change input', function () {
        // Check the data of the input and compare against the data that was searalized
        // If the data matches then disable the button
        $(this).find('#saveButton').prop('disabled', $(this).serialize() === $(this).data('serialized'));
    }).find('#saveButton').prop('disabled', true);

    //Prompts user if they are sure about delete action
    $(function () {
        $('#deleteVehicleLink').click(function () {
            return window.confirm("Are you sure? You want to delete Vin #: " + $('#vinNum').val());

        });
    });
});