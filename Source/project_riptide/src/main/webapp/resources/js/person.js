$(document).ready(function () {
    // Disable the delete button on form load
$('#deleteLink').attr('disabled',true);
// This code makes the entire row a clickable link in the table showing the people
// Also highlights the row when you hover your mouse over it
    $(function () {
        $('.table tr[data-href]').each(function () {
            // Change the cursor to a pointed finger when hovering
            $(this).css('cursor', 'pointer').hover(
                    function () {
                        // Add the bootstrap active class to the row
                        $(this).addClass('active');
                    },
                    function () {
                        // remove it when mouse is not hovering
                        $(this).removeClass('active');
                    }).click(function () {
                // The code for the clickable part
                document.location = $(this).attr('data-href');              
            }
            );
        });
    });

// Call the method that handles the ajax request to the controller when a row is clicked
    $(".personRow").click(function () {
        // Debug ******* Finding the ID of the person from the hidden field in the table
        //console.log('Id ******** ' + $(this).find('td#personIdRow').html());
        // Grab the value of the person ID from the table
        var id = $(this).find('td#personIdRow').html();
        // Call the get person details method and pass it the id
        getPersonDetails(id); 
        // Enable the delete button when a person is selected
        $('#deleteLink').attr('disabled',false);
    });

// this function is the ajax request to the controller
// calls the getDetails() method and passes it the id of the person clicked
// Returns a string which is converted to JSON and then to an object
    function getPersonDetails(id) {
        // console.log($('#personId').html());
        $.ajax({
            url: "/riptide/person/getDetails",
            type: 'GET',
            dataType: 'json',
            contentType: "application/json",
            data: {id: id},
            success: function (data) {
                // convert the response to JSON
                var jsonPerson = JSON.stringify(data);
                // Convert it to an object
                var personObject = jQuery.parseJSON(jsonPerson);

                //************** Debug ******************
                //console.log(personObject);


                // Debugging the ID grabbed from the AJAX call
                //console.log("Id from the database ***** " + personObject.personId);

                // fill the fields with the details of the object
                // the object will be the person returned from the database               
                $('#personId').val(personObject.personId);
                console.log($('#personId').val());
                $('#firstName').val(personObject.firstName);
                console.log($('#firstName').val());
                $('#middleName').val(personObject.middleName);
                $('#dateBox').val(personObject.dob);
                $('#lastName').val(personObject.lastName);
                $('#licenseNum').val(personObject.licenseNum);
                $('#province').val(personObject.province);
                $('#sex').val(personObject.sex);
                $('#address').val(personObject.address);
                $('#postalCode').val(personObject.postalCode);
                $('#phone').val(personObject.phoneNum);
                $('#cellPhone').val(personObject.alternateContact);
                $('#lawEnforcement').prop('checked', personObject.isLawEnforcement);
                // Loop through the radio buttons and match it to the association attribute
                // from the person. It will then check the correct radio button based on what
                // type the person is.
                $('input:radio').each(function () {
                    if ($(this).val() === personObject.association) {
                        $(this).prop('checked', true);
                    }
                });
                $('#race').val(personObject.race);
                $('#height').val(personObject.height);
                $('#hair').val(personObject.hairColor);
                $('#weight').val(personObject.weight);
                $('#eyeColor').val(personObject.eyeColor);
                // Add the href attribute to the delete link
                // Sends it through to the controller
                $("#deleteLink").attr("href", "/riptide/person/delete?id=" + personObject.personId);

            }
        });
    }

    // Tie a method call to clear the form when the add button is clicked
    $('#add').click(function () {
        clearForm();
        $('#firstName').focus();
        // ***************** Debugging *************************
        // Check the person ID after the form has been reset. Make sure it resets
        // Hidden fields are not resetting with the form.reset() in jQuery //
        console.log('Form reset personId#: ' + $('#personId').val());
        console.log('Form reset personId#: ' + $('#firstName').val());
        // Disable the delete button when creating a new person
        $('#deleteLink').attr('disabled',true);
    });

    // Functionality to reset the form
    // When the add button is clicked all the form fields will be reset to their
    // defaults
    function clearForm() {
        $('#personForm')[0].reset();
        // Fix for the bug that wont reset hidden form fields
        $('#personId').val('');
        $("#deleteLink").attr("href", "/riptide/person/delete?id=");
    }

    // This functionality here will disable the button
    // Loop through each input in the form
    $('form').each(function () {
        // Serialize the data and store as "searialized"
        $(this).data('serialized', $(this).serialize());
        // Using the .on detect a change to any of the input fields
    }).on('change input', function () {
        // Check the data of the input and compare against the data that was searalized
        // If the data matches then disable the button
        $(this).find('#saveButton').prop('disabled', $(this).serialize() === $(this).data('serialized'));
    }).find('#saveButton').prop('disabled', true);
    
        // Function which prompts the user if they are sure after clicking delete
    $(function() {
    $('#deleteLink').click(function() {
        return window.confirm("Are you sure? You want to delete " + $('#firstName').val() + " " + $('#lastName').val());
    });
});
});


