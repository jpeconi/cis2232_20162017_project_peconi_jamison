
/* global reponse */

$(document).ready(function () {
    $('#deletePropertyLink').attr('disabled', true);
// This code makes the entire row a clickable link in the table showing the properties
// Also highlights the row when you hover your mouse over it
    $(function () {
        $('.table tr[data-href]').each(function () {
            // Change the cursor to a pointed finger when hovering
            $(this).css('cursor', 'pointer').hover(
                    function () {
                        // Add the bootstrap active class to the row
                        $(this).addClass('active');
                    },
                    function () {
                        // remove it when mouse is not hovering
                        $(this).removeClass('active');
                    }).click(function () {
                // The code for the clickable part
                document.location = $(this).attr('data-href');
            }
            );
        });
    });

// Call the method that handles the ajax request to the controller when a row is clicked
    $(".propertyRow").click(function () {
        // Debug ******* Finding the ID of the property from the hidden field in the table
        //console.log('Id ******** ' + $(this).find('td#propertyIdRow').html());
        // Grab the value of the property ID from the table
        var id = $(this).find('td#propertyIdRow').html();
        // Call the get property details method and pass it the id
        getPropertyDetails(id);

        $('#deletePropertyLink').attr('disabled', false);
    });

// this function is the ajax request to the controller
// calls the getDetails() method and passes it the id of the property clicked
// Returns a string which is converted to JSON and then to an object
    function getPropertyDetails(id) {
        // console.log($('#propertyId').html());
        $.ajax({
            url: "/riptide/property/getDetails",
            type: 'GET',
            dataType: 'json',
            contentType: "application/json",
            data: {id: id},
            success: function (data) {
                // convert the response to JSON
                var jsonProperty = JSON.stringify(data);
                // Convert it to an object
                var propertyObject = jQuery.parseJSON(jsonProperty);

                //************** Debug ******************
                //console.log(properyObject);


                // Debugging the ID grabbed from the AJAX call
                console.log("Id from the database ***** " + propertyObject.propertyId);

                // fill the fields with the details of the object
                // the object will be the property returned from the database               
                $('#propertyId').val(propertyObject.propertyId);
                console.log($('#propertyId').val());
                $('#description').val(propertyObject.description);
                console.log($('#description').val());
                $('#quantity').val(propertyObject.quantity);
                $('#measurement').val(propertyObject.measurement);
                $('#seizedBy').val(propertyObject.seizedBy);
                $('#badgeNum').val(propertyObject.badgeNum);
                $('#dateSeized').val(propertyObject.dateSeized);
                console.log("This is the date seized " + propertyObject.dateSeized);
                $('#timeBox').val(propertyObject.timeSeized);
                console.log("This is the time seized " + propertyObject.timeSeized);
                console.log("This is wat we want" + propertyObject.timeSeized);
                $('#storageLocation').val(propertyObject.storageLocation);
                $('#entryDate').val(propertyObject.entryDate);
                $('#owner').val(propertyObject.owner);
                $("#deletePropertyLink").attr("href", "/riptide/property/delete?id=" + propertyObject.propertyId);

                console.log("We passed the deleteLink thing");

            }
        });
    }

    // Tie a method call to clear the form when the add button is clicked
    $('#add').click(function () {
        clearForm();
        $('#description').focus();
        // ***************** Debugging *************************
        // Check the description ID after the form has been reset. Make sure it resets
        // Hidden fields are not resetting with the form.reset() in jQuery //
        console.log('Form reset propertyId#: ' + $('#propertyId').val());
        console.log('Form reset propertyId#: ' + $('#description').val());
    });

    // Functionality to reset the form
    // When the add button is clicked all the form fields will be reset to their
    // defaults
    function clearForm() {
        $('#propertyForm')[0].reset();
        // Fix for the bug that wont reset hidden form fields
        $('#propertyId').val('');
        $("#deletePropertyLink").attr("href", "/riptide/property/delete?id=");
    }

    // This functionality here will disable the button
    // Loop through each input in the form
    $('form').each(function () {
        // Serialize the data and store as "searialized"
        $(this).data('serialized', $(this).serialize());
        // Using the .on detect a change to any of the input fields
    }).on('change input', function () {
        // Check the data of the input and compare against the data that was searalized
        // If the data matches then disable the button
        $(this).find('#saveButton').prop('disabled', $(this).serialize() === $(this).data('serialized'));
    }).find('#saveButton').prop('disabled', true);

    $(function () {
        $('#deletePropertyLink').click(function () {
            return window.confirm("Are you sure? You want to delete " + $('#description').val());
        });
    });
});


