
INSERT INTO `person` (`personId`, `incidentNum`, `firstName`, `middleName`, `lastName`, `dob`, `licenseNum`, `province`, `address`, `postalCode`, `phoneNum`, `alternateContact`, `sex`, `race`, `height`, `weight`, `hairColor`, `eyeColor`, `association`, `isLawEnforcement`) VALUES
(1, 1, 'Jamison', 'N', 'Peconi', '2017-01-11', '2340928342', 'PE', '36-2 Miah dr', 'c0a1h3', '9026266155', '9026266155', 'male', 'white', '6''4', 220, 'brown', 'blue', 'reporting', 1),
(2, 1, 'Bill', 'J', 'Johnson', '2017-01-01', '234234234234', 'PE', '58 Fairview Ln', 'c0a1e0', '9026266155', '9026266155', 'male', 'black', '5''9', 290, 'red', 'green', 'other', 0);
