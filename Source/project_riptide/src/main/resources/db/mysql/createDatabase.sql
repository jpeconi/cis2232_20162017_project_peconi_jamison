-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 14, 2016 at 03:06 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `incidents`
--

-- --------------------------------------------------------
drop database if exists cis2232_incidents;
create database cis2232_incidents;
use cis2232_incidents;
--
-- Table structure for table `codetype`
--

CREATE TABLE `codetype` (
  `CodeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `codetype`
--

INSERT INTO `codetype` (`CodeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(4, 'Colors', 'Colors FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');
-- --------------------------------------------------------

--
-- Table structure for table `codevalue`
--

CREATE TABLE `codevalue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Dumping data for table `codevalue`
--

INSERT INTO `codevalue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 2, 'Murder', 'Murder', 'MurderFR', 'MurderFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 3, 'Robbery', 'Robbery', 'RobberyFR', 'RobberyFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(2, 4, 'Assault', 'Asssualt', 'AssualtFR', 'AssualtFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 5, 'Domestic Violence', 'Dom. Violence', 'Domestic ViolenceFR', 'Dom. ViolenceFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 6, 'Gang Related', 'Gang Rel.', 'Gang RelatedFR', 'Gang Rel.FR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 7, 'Force Used', 'Force', 'Force UsedFR', 'ForceFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 8, 'Press Release', 'Press Rel.', 'Press ReleaseFR', 'Press Rel.FR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 9, 'Victim Senior Citzen', 'Vic. Sen. Citizen', 'Victim Senior CitizenFR', 'Vic. Sen. CitizenFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 10, 'Hate Crime', 'Hate Crime', 'Hate CrimeFR', 'Hate CrimeFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 11, 'Pursuit', 'Pursuit', 'PursuitFR', 'PursuitFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(3, 12, 'Child Abuse', 'Child Abuse', 'Child AbuseFR', 'Child AbuseFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 13, 'Red', 'Red', 'Rouge', 'Rouge', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 14, 'Blue', 'Blue', 'Bleu', 'Bleu', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 15, 'Green', 'Green', 'Vert', 'Vert', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 16, 'Black', 'Black', 'Noir', 'Noir', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin'),
(4, 17, 'White', 'White', 'Blanc', 'Blanc', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `incident`
--

CREATE TABLE `incident` (
  `incidentNum` int(4) NOT NULL COMMENT 'Incident Number to be unique',
  `incidentType` varchar(10) DEFAULT NULL,
  `isAttempted` tinyint(1) DEFAULT NULL,
  `endDate` varchar(10) DEFAULT NULL,
  `startDate` varchar(10) NOT NULL,
  `endTime` varchar(10) NOT NULL,
  `startTime` varchar(10) NOT NULL,
  `location` varchar(40) NOT NULL,
   `additionalOffenses` varchar(255) NULL,
  `dateCreated` varchar(10) DEFAULT NULL,
  `timeCreated` varchar(10) DEFAULT NULL,
  `officer` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `incident`
--


INSERT INTO `incident` (`incidentNum`, `incidentType`, `isAttempted`, `endDate`, `startDate`, `endTime`, `startTime`, `location`, `additionalOffenses`, `dateCreated`, `timeCreated`, `officer`) VALUES
(1, 'Battery', 0, '2016/10/16', '2016/10/17', '8:00AM', '12:00PM', 'Location', 'Domestic Violence', '2016/10/16', '8:00AM', 'Lt. Sanchez');



-- --------------------------------------------------------

--
-- Table structure for table `officer`
--

CREATE TABLE `officer` (
  `userAccessId` int(3) NOT NULL,
  `firstName` varchar(120) NOT NULL,
  `lastName` varchar(120) NOT NULL,
  `positionLong` varchar(140) NOT NULL,
  `positionShort` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `officer`
--

INSERT INTO `officer` (`userAccessId`, `firstName`, `lastName`, `positionLong`, `positionShort`) VALUES
(0, 'Bill', 'SMith', 'Captain', 'cpt');

-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE `person` (
  `personId` int(11) NOT NULL,
  `incidentNum` int(11) DEFAULT NULL,
  `firstName` varchar(255) NOT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) NOT NULL,
  `dob` date DEFAULT NULL,
  `licenseNum` varchar(12) DEFAULT NULL,
  `province` varchar(2) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `postalCode` varchar(6) DEFAULT NULL,
  `phoneNum` varchar(12) DEFAULT NULL,
  `alternateContact` varchar(255) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `race` varchar(10) DEFAULT NULL,
  `height` varchar(4) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `hairColor` varchar(12) DEFAULT NULL,
  `eyeColor` varchar(12) DEFAULT NULL,
  `association` varchar(12) NOT NULL,
  `isLawEnforcement` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `propertyId` int(11) NOT NULL,
  `incidentNum` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `description` varchar(255) NOT NULL,
  `measurement` varchar(10) DEFAULT NULL,
  `seizedBy` varchar(30) DEFAULT NULL,
  `badgeNum` varchar(12) DEFAULT NULL,
  `dateSeized` date NOT NULL,
  `timeSeized` varchar(255) NOT NULL,
  `storageLocation` varchar(255) NOT NULL,
  `entryDate` date NOT NULL,
  `owner` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `useraccess`
--

CREATE TABLE `useraccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `useraccess`
--

INSERT INTO `useraccess` (`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) VALUES
(0, 'admin', 'test', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `vinNum` varchar(17) NOT NULL,
  `incidentNum` int(4) NOT NULL,
  `licensePlate` varchar(12) DEFAULT NULL,
  `province` varchar(2) DEFAULT NULL,
  `tagRegistration` varchar(4) DEFAULT NULL,
  `year` int(4) NOT NULL,
  `type` varchar(10) NOT NULL,
  `make` varchar(12) NOT NULL,
  `model` varchar(12) NOT NULL,
  `style` varchar(12) NOT NULL,
  `primeColor` varchar(12) NOT NULL,
  `secondColor` varchar(12) DEFAULT NULL,
  `destroyed` tinyint(1) NOT NULL,
  `stolen` tinyint(1) NOT NULL,
  `timeReported` date DEFAULT NULL,
  `recovered` tinyint(1) NOT NULL,
  `dateRecovered` date DEFAULT NULL,
  `stored` tinyint(1) NOT NULL,
  `impounded` tinyint(1) NOT NULL,
  `dateImpounded` date DEFAULT NULL,
  `leftAtScene` tinyint(1) NOT NULL,
  `released` tinyint(1) NOT NULL,
  `isTowed` tinyint(1) NOT NULL,
  `locationTowed` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `regOwner` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `codetype`
--
ALTER TABLE `codetype`
  ADD PRIMARY KEY (`CodeTypeId`);

--
-- Indexes for table `codevalue`
--
ALTER TABLE `codevalue`
  ADD PRIMARY KEY (`codeValueSequence`);

--
-- Indexes for table `incident`
--
ALTER TABLE `incident`
  ADD PRIMARY KEY (`incidentNum`);

--
-- Indexes for table `officer`
--
ALTER TABLE `officer`
  ADD PRIMARY KEY (`userAccessId`);

--
-- Indexes for table `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`personId`),
  ADD KEY `incidentNum` (`incidentNum`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`propertyId`),
  ADD KEY `incidentNum` (`incidentNum`);

--
-- Indexes for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD PRIMARY KEY (`userAccessId`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`vinNum`),
  ADD KEY `incidentNum` (`incidentNum`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `codetype`
--
ALTER TABLE `codetype`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `codevalue`
--
ALTER TABLE `codevalue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `incident`
--
ALTER TABLE `incident`
  MODIFY `incidentNum` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Incident Number to be unique', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `person`
--
ALTER TABLE `person`
  MODIFY `personId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `propertyId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `useraccess`
--
ALTER TABLE `useraccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
