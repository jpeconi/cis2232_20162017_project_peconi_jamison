package info.hccis.riptide.web;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class OtherController {

    @RequestMapping("/redirect")
    public ModelAndView redirect(Model model, HttpServletRequest request){
        System.out.println("Redirect");
        String redirect = "";
        try{
            int choice = Integer.parseInt(request.getParameter("choice"));
            System.out.println("*******" + choice);
            switch(choice){
                case 1:
                    redirect = "incidentShow";
                    break;
                case 2:
                    redirect = "person";
                    break;
                case 3:
                    redirect = "property";
                    break;
                case 4:
                    redirect = "vehicle";
                    break;
            }
        }catch(Exception e){
            
        }   
        
        request.getSession().setAttribute("incidentID", request.getParameter("id"));
        
        return new ModelAndView("redirect:/" + redirect);
    }



}
