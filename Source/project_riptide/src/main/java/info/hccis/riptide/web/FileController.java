package info.hccis.riptide.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Jamison Peconi
 * @since 11/23/2016
 * @purpose This controller will handle the functionality for the user to
 * download the PDF files from the server. This page will contain the methods
 * associated with the files downloading for now (Uploading after Christmas in
 * the revision).
 */
@Controller
public class FileController extends HttpServlet {

    /**
     * @author Jamison Peconi
     * @since 11/23/2016
     * @param model
     * @param request
     * @param response
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     * @purpose This method will handle the requests made to the documents page.
     * When the user visits the documents page this controller will be called
     * upon. If the user is visiting the page for the first time there will be
     * no file downloaded the static page will simply be displayed. In the event
     * the user has selected a form to download, this page will just refresh to
     * itself but will prompt the user to download the form they have selected.
     */
    @RequestMapping("/files")
    public String downloadFiles(Model model, HttpServletRequest request, HttpServletResponse response) throws FileNotFoundException, IOException {
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        model.addAttribute("incidentID", incidentNum);
        // System.out.println("The real path ********** " + getServletContext().getRealPath("/"));
        String contextPath = request.getContextPath();
        System.out.println(contextPath);
        // Check to see if the file parameter is null
        // If it's not, download the form that is requested
        if (request.getParameter("file") != null) {
            // Grab the filename from the request made by the user
            String fileName = request.getParameter("file");
            // ******************* Debugs ******************
            System.out.println(fileName);
            // Build the path to the files
            String filePath = "/Users/jayp/Desktop/" + fileName + ".pdf";
            File file = null;
            // Assign a file object to the path of the file
            file = new File(filePath);

            // Uncomment this if you want the pdf to download to the client as oppose to displaying in the browser
            //response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", file.getName()));
            // This code makes the pdf display in the browser
            response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));
            response.setContentLength((int) file.length());
            // Get input stream from the file
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            //Copy bytes from source to destination(outputstream in this example), closes both streams.
            FileCopyUtils.copy(inputStream, response.getOutputStream());
            return "riptide/files";
        } else {
            // No form requested load the page with the list of forms
            return "riptide/files";
        }
    }

}
