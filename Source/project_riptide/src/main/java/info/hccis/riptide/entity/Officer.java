/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jayp
 */
@Entity
@Table(name = "officer")
@NamedQueries({
    @NamedQuery(name = "Officer.findAll", query = "SELECT o FROM Officer o")})
public class Officer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userAccessId")
    private Integer userAccessId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "firstName")
    private String firstName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 120)
    @Column(name = "lastName")
    private String lastName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 140)
    @Column(name = "positionLong")
    private String positionLong;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "positionShort")
    private String positionShort;

    public Officer() {
    }

    public Officer(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public Officer(Integer userAccessId, String firstName, String lastName, String positionLong, String positionShort) {
        this.userAccessId = userAccessId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.positionLong = positionLong;
        this.positionShort = positionShort;
    }

    public Integer getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPositionLong() {
        return positionLong;
    }

    public void setPositionLong(String positionLong) {
        this.positionLong = positionLong;
    }

    public String getPositionShort() {
        return positionShort;
    }

    public void setPositionShort(String positionShort) {
        this.positionShort = positionShort;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAccessId != null ? userAccessId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Officer)) {
            return false;
        }
        Officer other = (Officer) object;
        if ((this.userAccessId == null && other.userAccessId != null) || (this.userAccessId != null && !this.userAccessId.equals(other.userAccessId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.riptide.entity.Officer[ userAccessId=" + userAccessId + " ]";
    }
    
}
