/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Elkeno
 */
@Entity
@Table(name = "incident")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incident.findAll", query = "SELECT i FROM Incident i")})
public class Incident implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incidentNum")
    private Integer incidentNum;
    @Size(max = 10)
    @Column(name = "incidentType")
    private String incidentType;
    @Column(name = "isAttempted")
    private Boolean isAttempted;
    @Size(max = 10)
    @Column(name = "endDate")
    private String endDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "startDate")
    private String startDate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "endTime")
    private String endTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "startTime")
    private String startTime;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "location")
    private String location;
    @Size(max = 255)
    @Column(name = "additionalOffenses")
    private String additionalOffenses;
    @Size(max = 10)
    @Column(name = "dateCreated")
    private String dateCreated;
    @Size(max = 10)
    @Column(name = "timeCreated")
    private String timeCreated;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "officer")
    private String officer;
@Transient
    private String[] offenses;
    public void setOffenses(String[] offenses){
        this.offenses = offenses;
    }

    public Incident() {
    }

    public Incident(Integer incidentNum) {
        this.incidentNum = incidentNum;
    }

    public Incident(Integer incidentNum, String startDate, String endTime, String startTime, String location, String officer) {
        this.incidentNum = incidentNum;
        this.startDate = startDate;
        this.endTime = endTime;
        this.startTime = startTime;
        this.location = location;
        this.officer = officer;
    }

    public Integer getIncidentNum() {
        return incidentNum;
    }

    public void setIncidentNum(Integer incidentNum) {
        this.incidentNum = incidentNum;
    }

    public String getIncidentType() {
        return incidentType;
    }

    public void setIncidentType(String incidentType) {
        this.incidentType = incidentType;
    }

    public Boolean getIsAttempted() {
        return isAttempted;
    }

    public void setIsAttempted(Boolean isAttempted) {
        this.isAttempted = isAttempted;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAdditionalOffenses() {
        return additionalOffenses;
    }

    public void setAdditionalOffenses(String additionalOffenses) {
        this.additionalOffenses = additionalOffenses;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getOfficer() {
        return officer;
    }

    public void setOfficer(String officer) {
        this.officer = officer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incidentNum != null ? incidentNum.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incident)) {
            return false;
        }
        Incident other = (Incident) object;
        if ((this.incidentNum == null && other.incidentNum != null) || (this.incidentNum != null && !this.incidentNum.equals(other.incidentNum))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.riptide.entity.Incident[ incidentNum=" + incidentNum + " ]";
    }
    
}
