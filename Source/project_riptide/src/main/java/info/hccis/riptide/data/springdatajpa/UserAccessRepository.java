package info.hccis.riptide.data.springdatajpa;


import info.hccis.riptide.useraccess.entity.Useraccess;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAccessRepository extends CrudRepository<Useraccess, Integer> {
    Useraccess findByUsername(String username);
}