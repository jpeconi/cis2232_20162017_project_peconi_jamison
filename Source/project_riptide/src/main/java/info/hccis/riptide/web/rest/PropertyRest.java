package info.hccis.riptide.web.rest;


import info.hccis.riptide.data.springdatajpa.PropertyRepository;
import info.hccis.riptide.entity.Property;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @author Jamison Peconi
 * @since 11/18/2016
 * 
 * This class will act as the Rest web service for the people in the riptide
 * project. This class contains methods which map all the different endpoints
 * and their functionality.
 */
@Path("PropertyService")
public class PropertyRest { 

    @Resource
    private final PropertyRepository propertyRepository;

    public PropertyRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.propertyRepository = applicationContext.getBean(PropertyRepository.class);
    }

    /**
     * @author Jamison Peconi
     * @since 11/18/2016
     * @return
     *
     * This endpoint will return a list of all the players to the caller. It
     * will return all the players in the form of a JSON array. This method is
     * called when a GET request is sent to /people
     */
    @GET
    @Path("/properties")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {

        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(propertyRepository.findAll());
        } catch (IOException ex) {
            Logger.getLogger(PersonRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    /**
     * @author Jamison Peconi
     * @since 11/18/2016
     * @param id
     * @return
     *
     * This endpoint will return a specific player based on the id that is
     * passed into the GET request in the URL. Returns JSON
     */
    @GET
    @Path("properties/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {

        Property property = propertyRepository.findOne(id);
        if (property == null) {
            return Response.status(204).entity("{}").build();
        }
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(property);
        } catch (IOException ex) {
            Logger.getLogger(PersonRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    /**
     * @author Jamison Peconi
     * @param jsonIn
     * @throws java.io.IOException
     * @since 11/18/2016
     * @return 
     * 
     * This endpoint will add a person to the database. This endpoint accepts a
     * person from a POST request and adds them to the person table in the db.
     * This method will be called when a user hits the people endpoint with a 
     * POST.
     */
    @POST
    @Path("/properties")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCamper(String jsonIn) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        //JSON from String to Object
        Property property = mapper.readValue(jsonIn, Property.class);
        property = propertyRepository.save(property);
        String temp = "";
        try {
            temp = mapper.writeValueAsString(property);
        } catch (IOException ex) {
            Logger.getLogger(PersonRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(201).entity(temp).build();
    }

}


    

