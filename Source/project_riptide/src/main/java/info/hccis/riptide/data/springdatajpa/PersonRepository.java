package info.hccis.riptide.data.springdatajpa;


import info.hccis.riptide.entity.Person;
import java.util.List;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, Integer> {
    List<Person> findByIncidentNum(int incidentNum);
    @Query(value = "select * from person where firstName LIKE %?1% OR lastName LIKE %?1%",nativeQuery = true)
    List<Person> findByFirstnameContains(String firstName);
    
}