package info.hccis.riptide.data.springdatajpa;

import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import info.hccis.riptide.useraccess.entity.Codevalue;
import java.util.List;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Elkeno
 */
@Repository
public interface CodeValueRepository extends CrudRepository<Codevalue, Integer>{
    List<Codevalue> findByCodeTypeId(int codeTypeId);
}
