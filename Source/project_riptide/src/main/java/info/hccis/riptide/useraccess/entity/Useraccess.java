/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.useraccess.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jpeconi
 */
@Entity
@Table(name = "useraccess")
@NamedQueries({
    @NamedQuery(name = "Useraccess.findAll", query = "SELECT u FROM Useraccess u")})
public class Useraccess implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userAccessId")
    private Integer userAccessId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "password")
    private String password;
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    @JoinColumn(name = "userTypeCode", referencedColumnName = "codeValueSequence")
    @ManyToOne(optional = false)
    private Codevalue userTypeCode;

    public Useraccess() {
    }

    public Useraccess(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public Useraccess(Integer userAccessId, String username, String password) {
        this.userAccessId = userAccessId;
        this.username = username;
        this.password = password;
    }

    public Integer getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Codevalue getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(Codevalue userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAccessId != null ? userAccessId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Useraccess)) {
            return false;
        }
        Useraccess other = (Useraccess) object;
        if ((this.userAccessId == null && other.userAccessId != null) || (this.userAccessId != null && !this.userAccessId.equals(other.userAccessId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.riptide.useraccess.entity.Useraccess[ userAccessId=" + userAccessId + " ]";
    }
    
}
