package info.hccis.riptide.web;

import info.hccis.riptide.data.springdatajpa.PersonRepository;
import info.hccis.riptide.data.springdatajpa.UserAccessRepository;
import info.hccis.riptide.entity.Person;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author jpeconi
 * @since 12/14/2016
 * @purpose This controller handles all the functionality to do with people.
 * This page contains all the methods make the person page work.
 */
@Controller
public class PersonController {

    private final UserAccessRepository userAccessRepository;
    private final PersonRepository personRepository;

    @Autowired
    public PersonController(UserAccessRepository userAccessRepository, PersonRepository personRepository) {
        this.userAccessRepository = userAccessRepository;
        this.personRepository = personRepository;
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @purpose This method will be the root for the person page. This method
     * handles the functionality when the user clicks the add person from the
     * main menu.
     */
    @RequestMapping("/person")
    public String showPersonPage(Model model, HttpServletRequest request) {
        // Grab the incident ID that we are currently working on
        // This is gonna be used to add the people to the right incident
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        System.out.println("**************************\nPerson:" + incidentNum);
        try {
            // Get all the people from the particular incident
            ArrayList<Person> persons = (ArrayList<Person>) personRepository.findByIncidentNum(incidentNum);
            // add the list to the model
            model.addAttribute("persons", persons);
        } catch (Exception e) {
            System.out.println(e);
        }
        // Add the incident ID to the model
        model.addAttribute("incidentID", incidentNum);
        // Create a person for the form
        Person temp = new Person();
        // Add the person object to the model
        model.addAttribute("person", temp);

        return "riptide/person";
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @purpose This method is for the AJAX request from the person page. This
     * method will accept the ID from the GET request. It will then pass that ID
     * to find the person with that id. It then returns json back to the html
     * page. This will be parsed by JS.
     */
    @RequestMapping(value = "/person/getDetails", headers = "Accept=*/*", method = RequestMethod.GET)
    public @ResponseBody
    Person getDetails(@RequestParam("id") String id, Model model) {
        // Search for a particular person using the id from the GET parameter
        Person result = personRepository.findOne(Integer.parseInt(id));
        return result;
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @purpose This method will handle the add functionality. This accepts the
     * person that is bound to the form and adds that person to the database.
     * This method also contains error checking in the event the user put in
     * wrong data while filling out the form.
     */
    @RequestMapping("/person/add")
    public String addPerson(Model model, @ModelAttribute("person") @Valid Person person, BindingResult bindingResult, HttpServletRequest request) {
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        System.out.println("**************** DOB - " + person.getDob());
        // Check to see if the form data has errors
        if (bindingResult.hasErrors()) {
            // Return to the form page if so          
            ArrayList<Person> persons = (ArrayList<Person>) personRepository.findByIncidentNum(incidentNum);
            // Grab the incident ID that we are currently working on
            // This is gonna be used to add the people to the right incident
            model.addAttribute("incidentID", incidentNum);
            model.addAttribute("persons", persons);
            return "riptide/person";
        }

        personRepository.save(person);
        ArrayList<Person> persons = (ArrayList<Person>) personRepository.findByIncidentNum(incidentNum);
        // Grab the incident ID that we are currently working on
        // This is gonna be used to add the people to the right incident
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("persons", persons);
        Person temp = new Person();
        model.addAttribute("person", temp);
        return "riptide/person";
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @purpose This method will handle the delete functionality. This method
     * will grab the id from a get request and pass it to the delete query. This
     * will delete a person from the database and return the user back to the
     * person page.
     */
    @RequestMapping("/person/delete")
    public String deletePersonPage(Model model, HttpServletRequest request) {
        // Debugging
        System.out.println("The id is *********************************** " + request.getParameter("id"));

        // Grab the incident ID
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        // Grab the list of persons attached to the incident
        ArrayList<Person> persons = (ArrayList<Person>) personRepository.findByIncidentNum(incidentNum);

        // Conditional statement to reroute back to the page if they hit delete without selecting a person
        if (request.getParameter("id") == null || request.getParameter("id").isEmpty()) {
            model.addAttribute("incidentID", incidentNum);
            model.addAttribute("persons", persons);
            Person temp = new Person();
            model.addAttribute("person", temp);
            return "riptide/person";
        }
        // Delete query
        personRepository.delete(Integer.parseInt(request.getParameter("id")));
        persons = (ArrayList<Person>) personRepository.findByIncidentNum(incidentNum);
        // Add the data back to the model
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("persons", persons);
        Person temp = new Person();
        model.addAttribute("person", temp);
        return "riptide/person";
    }
}
