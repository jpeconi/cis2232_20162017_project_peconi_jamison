/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.entity;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author pfouchere
 */
@Entity
@Table(name = "vehicle")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vehicle.findAll", query = "SELECT v FROM Vehicle v")})
    
public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "vinNum")
    private String vinNum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "incidentNum")
    private int incidentNum;
    @Size(max = 12)
    @Column(name = "licensePlate")
    private String licensePlate;
    @Size(max = 2)
    @Column(name = "province")
    private String province;
    @Size(max = 4)
    @Column(name = "tagRegistration")
    private String tagRegistration;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    private int year;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "make")
    private String make;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "model")
    private String model;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "style")
    private String style;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "primeColor")
    private String primeColor;
    @Size(max = 12)
    @Column(name = "secondColor")
    private String secondColor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "destroyed")
    private boolean destroyed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stolen")
    private boolean stolen;
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "timeReported")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date timeReported;
    @Basic(optional = false)
    @NotNull
    @Column(name = "recovered")
    private boolean recovered;
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateRecovered")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateRecovered;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stored")
    private boolean stored;
    @Basic(optional = false)
    @NotNull
    @Column(name = "impounded")
    private boolean impounded;
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateImpounded")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateImpounded;
    @Basic(optional = false)
    @NotNull
    @Column(name = "leftAtScene")
    private boolean leftAtScene;
    @Basic(optional = false)
    @NotNull
    @Column(name = "released")
    private boolean released;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isTowed")
    private boolean isTowed;
    @Size(max = 255)
    @Column(name = "locationTowed")
    private String locationTowed;
    @Size(max = 255)
    @Column(name = "comment")
    private String comment;
    @Size(max = 255)
    @Column(name = "regOwner")
    private String regOwner;

    public Vehicle() {
    }

    public Vehicle(String vinNum) {
        this.vinNum = vinNum;
    }

    public Vehicle(String vinNum, int incidentNum, int year, String type, String make, String model, String style, String primeColor, boolean destroyed, boolean stolen, boolean recovered, boolean stored, boolean impounded, boolean leftAtScene, boolean released, boolean isTowed) {
        this.vinNum = vinNum;
        this.incidentNum = incidentNum;
        this.year = year;
        this.type = type;
        this.make = make;
        this.model = model;
        this.style = style;
        this.primeColor = primeColor;
        this.destroyed = destroyed;
        this.stolen = stolen;
        this.recovered = recovered;
        this.stored = stored;
        this.impounded = impounded;
        this.leftAtScene = leftAtScene;
        this.released = released;
        this.isTowed = isTowed;
    }

    public String getVinNum() {
        return vinNum;
    }

    public void setVinNum(String vinNum) {
        this.vinNum = vinNum;
    }

    public int getIncidentNum() {
        return incidentNum;
    }

    public void setIncidentNum(int incidentNum) {
        this.incidentNum = incidentNum;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getTagRegistration() {
        return tagRegistration;
    }

    public void setTagRegistration(String tagRegistration) {
        this.tagRegistration = tagRegistration;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getPrimeColor() {
        return primeColor;
    }

    public void setPrimeColor(String primeColor) {
        this.primeColor = primeColor;
    }

    public String getSecondColor() {
        return secondColor;
    }

    public void setSecondColor(String secondColor) {
        this.secondColor = secondColor;
    }

    public boolean getDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    public boolean getStolen() {
        return stolen;
    }

    public void setStolen(boolean stolen) {
        this.stolen = stolen;
    }

    public Date getTimeReported() {
        return timeReported;
    }

    public void setTimeReported(Date timeReported) {
        this.timeReported = timeReported;
    }

    public boolean getRecovered() {
        return recovered;
    }

    public void setRecovered(boolean recovered) {
        this.recovered = recovered;
    }

    public Date getDateRecovered() {
        return dateRecovered;
    }

    public void setDateRecovered(Date dateRecovered) {
        this.dateRecovered = dateRecovered;
    }

    public boolean getStored() {
        return stored;
    }

    public void setStored(boolean stored) {
        this.stored = stored;
    }

    public boolean getImpounded() {
        return impounded;
    }

    public void setImpounded(boolean impounded) {
        this.impounded = impounded;
    }

    public Date getDateImpounded() {
        return dateImpounded;
    }

    public void setDateImpounded(Date dateImpounded) {
        this.dateImpounded = dateImpounded;
    }

    public boolean getLeftAtScene() {
        return leftAtScene;
    }

    public void setLeftAtScene(boolean leftAtScene) {
        this.leftAtScene = leftAtScene;
    }

    public boolean getReleased() {
        return released;
    }

    public void setReleased(boolean released) {
        this.released = released;
    }

    public boolean getIsTowed() {
        return isTowed;
    }

    public void setIsTowed(boolean isTowed) {
        this.isTowed = isTowed;
    }

    public String getLocationTowed() {
        return locationTowed;
    }

    public void setLocationTowed(String locationTowed) {
        this.locationTowed = locationTowed;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRegOwner() {
        return regOwner;
    }

    public void setRegOwner(String regOwner) {
        this.regOwner = regOwner;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (vinNum != null ? vinNum.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vehicle)) {
            return false;
        }
        Vehicle other = (Vehicle) object;
        if ((this.vinNum == null && other.vinNum != null) || (this.vinNum != null && !this.vinNum.equals(other.vinNum))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.riptide.entity.Vehicle[ vinNum=" + vinNum + " ]";
    }

}
