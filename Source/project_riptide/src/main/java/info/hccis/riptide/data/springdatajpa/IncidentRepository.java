package info.hccis.riptide.data.springdatajpa;

import info.hccis.riptide.entity.Incident;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Elkeno Jones_ejones109029
 * Date 20161110
 */
@Repository
public interface IncidentRepository extends CrudRepository<Incident, Integer>{
    List<Incident> findAll();
    
//    Incident save(Incident incident);
    
    @Query(value="select max(incidentNum) from incident", nativeQuery = true)
    Integer selectMaxId();
}
