package info.hccis.riptide.web;

import info.hccis.riptide.data.springdatajpa.IncidentRepository;
import info.hccis.riptide.data.springdatajpa.UserAccessRepository;
import info.hccis.riptide.entity.Incident;
import info.hccis.riptide.useraccess.entity.Useraccess;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jpeconi
 * @since 12/13/2016
 * @purpose This controller will be the default controller for the riptide
 * application. This controller contains general methods for use in the
 * application
 */
@Controller
public class RiptideController {

    // Repositories
    private final UserAccessRepository userAccessRepository;
    private final IncidentRepository ir;

    @Autowired
    public RiptideController(UserAccessRepository userAccessRepository, IncidentRepository ir) {
        this.userAccessRepository = userAccessRepository;
        this.ir = ir;
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @param model
     * @param request
     * @return
     * @purpose This controller is the root of the application. This will bring
     * up the login screen. This method also contains a redirect if the user is
     * already logged in. It will send them back to the main page.
     */
    @RequestMapping("/")
    public String showLogin(Model model, HttpServletRequest request) {
        // Set a user object to the object stored in the session
        Useraccess user = (Useraccess) (request.getSession().getAttribute("user"));
        // Check to see if the user is null
        // If so redirect to the login page
        if (user == null) {
            user = new Useraccess();
            model.addAttribute("user", user);
            return "riptide/auth";
        } else {
            // if the user session is set add the user and direct to the list page
            //System.out.println(request.getAttribute("user"));
            ArrayList<Incident> incidents = (ArrayList<Incident>) ir.findAll();
            model.addAttribute("incidents", incidents);

            return "riptide/listIncidents";
        }
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @param model
     * @param user
     * @param request
     * @return
     * @purpose This method handles the validation for the login. This method
     * will redirect the user if they login successfully. If they do not provide
     * proper credentials they will be returned to the login form with an error
     * message. This method will also write the username and a timestamp when
     * they successfully login.
     */
    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") Useraccess user, HttpServletRequest request) {
        // Assign a variable for the message to be displayed for bad login attempts
        String message = "";
        // Go to the database and find a user that matches the username entered in the form
        try {
            // Create a Useraccess object and assign it to one returned from the repository
            // Pass in the username entered from the login form.
            Useraccess temp = (Useraccess) userAccessRepository.findByUsername(user.getUsername());
            // If they usernames match
            if (user.getUsername().equals(temp.getUsername())) {
                // Check the passwords match
                if (user.getPassword().equals(temp.getPassword())) {
                    // Add the user to the session
                    request.getSession().setAttribute("user", temp);
                    // Get the user ID
                    int id = temp.getUserAccessId();
                    // Set the ID in the session
                    request.getSession().setAttribute("userId", id);

                    System.out.println("*********\n User id: " + id);
                    // Get the incidents and add them to the model
                    ArrayList<Incident> incidents = (ArrayList<Incident>) ir.findAll();
                    model.addAttribute("incidents", incidents);
                    model.addAttribute("user", temp);

                    // Added this functionality to write all the logins to a text file
                    // This will simply write the date and the username to every successful
                    // login attempt
                    // Login String
                    String loginString = "User: " + user.getUsername() + " Logged in at **** " + new Date();
                    BufferedWriter output = null;
                    File file;

                    try {
                        // Get the operating system
                        String os = System.getProperty("os.name").toLowerCase();
                        // Had to add this to make the file writing function for both Mac OS X and Windows
                        // I am only able to test this at home using Mac
                        // However, the code looks like it should work. Works great for me on the mac. 
                        // Will create the file on my desktop on the max os x
                        if (os.contains("mac")) { 
                            // Setup a file and give it the path
                            file = new File(System.getProperty("user.home") + "/Desktop/login.txt");
                        } else {
                            file = new File("C:\\login.txt");
                        }
                        // Create a file writer and buffered writer
                        // Set it to append using true flag
                        output = new BufferedWriter(new FileWriter(file, true));
                        // Write the string setup as well as a new line
                        output.write(loginString + "\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        if (output != null) {
                            output.close();
                        }
                    }
                    return "riptide/listIncidents";
                } else {
                    message = "Invalid password";
                    return "riptide/auth";
                }
            }
            // If there is no user found in the database with that username
        } catch (Exception e) {
            message = "Username not found";
            return "riptide/auth";
        } finally {
            // Send the message to the model
            model.addAttribute("message", message);
        }
        return null;
    }

    /**
     * @author Jamison Peconi
     * @since 12/14/2016
     * @param model
     * @param request
     * @return
     * @purpose THis method contains the functionality for logout. Simple method
     * that removes the user from the session.
     */
    @RequestMapping("/logout")
    public String logout(Model model, HttpServletRequest request) {
        // Destroy the session variable that is holding the user object
        request.getSession().removeAttribute("user");
        // Create a new null user to send back to the model
        Useraccess user = new Useraccess();
        model.addAttribute("user", user);
        // Redirect back to login page
        return "riptide/auth";
    }
}
