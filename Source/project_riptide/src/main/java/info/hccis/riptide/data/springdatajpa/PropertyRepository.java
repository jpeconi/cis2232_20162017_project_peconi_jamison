package info.hccis.riptide.data.springdatajpa;

import info.hccis.riptide.entity.Property;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Maders
 */

@Repository
public interface PropertyRepository extends CrudRepository<Property, Integer> {
    List<Property> findByIncidentNum(int incidentNum);

}
