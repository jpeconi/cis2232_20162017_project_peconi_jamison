package info.hccis.riptide.data.springdatajpa;

import info.hccis.riptide.entity.Officer;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Elkeno Jones_ejones109029
 */
@Repository
public interface OfficerRepository extends CrudRepository<Officer, Integer>{
    
}
