/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.web.rest;

import info.hccis.riptide.data.springdatajpa.VehicleRepository;
import info.hccis.riptide.entity.Vehicle;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Patrick Fouchere
 * @purpose Restful webservice for Vehicles.
 * Puts all vehicle data into a readable Json format
 */
@Path("VehicleService")
public class VehicleRest {

    @Resource
    private final VehicleRepository vehicleRepository;

    public VehicleRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.vehicleRepository = applicationContext.getBean(VehicleRepository.class);
    }

    @GET
    @Path("/vehicles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {

        System.out.println("in controller for /player/list");
        ArrayList<Vehicle> list = (ArrayList<Vehicle>) vehicleRepository.findAll();

        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(list);
        } catch (IOException ex) {
            Logger.getLogger(VehicleRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

}
