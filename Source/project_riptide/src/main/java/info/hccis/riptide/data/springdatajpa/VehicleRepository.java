package info.hccis.riptide.data.springdatajpa;

import info.hccis.riptide.entity.Vehicle;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Patrick Fouchere
 * @since December 1, 2016
 * Vehicle Repo that implements Crud repo. Also create a few queries to be used on the vehicle page.
 */
@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Integer>{
    List<Vehicle> findByIncidentNum(int incidentNum);
    List<Vehicle> findByVinNum(String vinNum);
    
    @Modifying
    @Transactional
    @Query(value="delete from Vehicle where vinNum = ?1", nativeQuery = true)
    void deleteByVinNum(String vinNum);
}
