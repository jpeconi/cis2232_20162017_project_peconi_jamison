/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.web;

import info.hccis.riptide.data.springdatajpa.PropertyRepository;
import info.hccis.riptide.data.springdatajpa.UserAccessRepository;
import info.hccis.riptide.entity.Property;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Maders
 */
@Controller
public class PropertyController {

    private final UserAccessRepository userAccessRepository;
    private final PropertyRepository propertyRepository;

    @Autowired
    public PropertyController(UserAccessRepository userAccessRepository, PropertyRepository propertyRepository) {
        this.userAccessRepository = userAccessRepository;
        this.propertyRepository = propertyRepository;
        
    }

    @RequestMapping("/property")
    public String showPropertyPage(Model model, HttpServletRequest request) {
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        ArrayList<Property> properties = (ArrayList<Property>) propertyRepository.findByIncidentNum(incidentNum);
        // Grab the incident ID that we are currently working on
        // This is gonna be used to add the people to the right incident
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("properties", properties);
        Property temp = new Property();
        model.addAttribute("property", temp);
        return "riptide/property";
    }

    @RequestMapping(value = "/property/getDetails", headers = "Accept=*/*", method = RequestMethod.GET)
    public @ResponseBody
    Property getDetails(@RequestParam("id") String id, Model model) {
        Property result = propertyRepository.findOne(Integer.parseInt(id));
        return result;
    }

    @RequestMapping("/property/add")
    public String addProperty(Model model, @ModelAttribute("property") @Valid Property property, BindingResult bindingResult, HttpServletRequest request) {
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        System.out.println("The date is *****************  " + property.getDateSeized());
        System.out.println();
        // Check to see if the form data has errors
        if (bindingResult.hasErrors()) {
            // Return to the form page if so          
            ArrayList<Property> properties = (ArrayList<Property>) propertyRepository.findByIncidentNum(incidentNum);
            // Grab the incident ID that we are currently working on
            // This is gonna be used to add the people to the right incident
            model.addAttribute("incidentID", incidentNum);
            model.addAttribute("properties", properties);
            return "riptide/property";
        }

        System.out.println("**************** Incident Num: " + property.getIncidentNum());
        System.out.println("**************** Description: " + property.getDescription());
        System.out.println("**************** BadgeNumber: " + property.getBadgeNum());

        propertyRepository.save(property);
        ArrayList<Property> properties = (ArrayList<Property>) propertyRepository.findByIncidentNum(incidentNum);
        // Grab the incident ID that we are currently working on
        // This is gonna be used to add the people to the right incident
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("properties", properties);
        Property temp = new Property();
        model.addAttribute("property", temp);
        return "riptide/property";

    }

    
    // Delete a property
    @RequestMapping("/property/delete")
    
    public String deletePropertyPage(Model model, HttpServletRequest request) {
        System.out.println("HEY YOU!!!!!!! HERE IS THE ID"+request.getParameter("id"));
        
        // Debugging
        System.out.println("We're in");
        System.out.println("The id is *********************************** " + request.getParameter("id"));
        
        // Grab the incident ID
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        // Grab the list of persons attached to the incident
        ArrayList<Property> properties = (ArrayList<Property>) propertyRepository.findByIncidentNum(incidentNum);
        
        // Conditional statement to reroute back to the page if they hit delete without selecting a property
        if (request.getParameter("id") == null || request.getParameter("id").isEmpty()) {
            model.addAttribute("incidentID", incidentNum);
            model.addAttribute("properties", properties);
            Property temp = new Property();
            model.addAttribute("property", temp);
            return "riptide/property";
        }
        // Delete query
        
        System.out.println("I made it to the delete query");
        propertyRepository.delete(Integer.parseInt(request.getParameter("id")));

        properties =(ArrayList<Property>) propertyRepository.findByIncidentNum(incidentNum);
        // Add the data back to the model
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("properties", properties);
        Property temp = new Property();
        model.addAttribute("property", temp);
        return "riptide/property";
    }
}
