package info.hccis.riptide.web;

import info.hccis.riptide.data.springdatajpa.CodeValueRepository;
import info.hccis.riptide.data.springdatajpa.IncidentRepository;
import info.hccis.riptide.data.springdatajpa.OfficerRepository;
import info.hccis.riptide.data.springdatajpa.UserAccessRepository;
import info.hccis.riptide.entity.Incident;
import info.hccis.riptide.useraccess.entity.Codevalue;
import info.hccis.riptide.entity.Officer;
import info.hccis.riptide.useraccess.entity.Useraccess;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jpeconi
 */
@Controller
public class IncidentController {

    private final UserAccessRepository userAccessRepository;
    private final CodeValueRepository codeRepo;
    private final IncidentRepository ir;
    private final OfficerRepository or;

    /**
     * Autowired controller used to inject dependencies
     * @author Elkeno Jones_ejones109029
     * Date: 20161114
     * 
     * @param userAccessRepository
     * @param codeRepo
     * @param ir
     * @param or 
     */
    @Autowired
    public IncidentController(UserAccessRepository userAccessRepository, CodeValueRepository codeRepo, IncidentRepository ir,
            OfficerRepository or) {
        this.userAccessRepository = userAccessRepository;
        this.codeRepo = codeRepo;
        this.ir = ir;
        this.or = or;
    }

    /**
     * Mapping controller for direct user to a new incident html page
     * @author Elkeno Jones_ejones109029
     * Date 20161114
     * 
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping("/addIncident")
    public String addIncident(Model model, HttpServletRequest request) {
        Useraccess user;
        try{
            user = (Useraccess) request.getSession().getAttribute("user");
            if (user != null){
                //Create list of code values for offense type and additional offenses
                ArrayList<Codevalue> codeValues = getIncidentTypes();
                ArrayList<Codevalue> extraOffenses = getAdditionalOffencts();

                // create player for to add to the model
                Incident newIncident = new Incident();
                int incidentNum = ir.selectMaxId() + 1;
                System.out.println("************\nIncident: " + incidentNum);

                // get the id of the officer that is logged in
                int userId = (int) request.getSession().getAttribute("userId");
                System.out.println("**********\n User num: " + userId 
                        + "\nSession - username: ");

                newIncident.setIncidentNum(incidentNum);
                Officer loggedInOfficer = (Officer) or.findOne(userId);

                newIncident.setOfficer(loggedInOfficer.getPositionShort() + ". " + loggedInOfficer.getFirstName());
                System.out.println("************\nOfficer: " + newIncident.getOfficer());

                model.addAttribute("incident", newIncident);
                model.addAttribute("codeValues", codeValues);
                model.addAttribute("officer", loggedInOfficer);
                model.addAttribute("extras", extraOffenses);

                return "riptide/addIncident";
            }
        } catch(IllegalStateException e){
            return "auth";
        }
        
        return "riptide/auth";
    }
    
    /**
     * Controller that will add the incident to the database
     * @author Elkeno Jones_ejones109029
     * 
     * @param model
     * @param incident
     * @param extras
     * @param result
     * @return 
     */
    @RequestMapping("/incidentAddSubmit")
    public String addSubmit(Model model, @RequestParam(value="extras") String[] extras, @Valid @ModelAttribute("incident") Incident incident, BindingResult result){
        System.out.println("**********\n"+incident.getIncidentType());
        
        if(result.hasErrors())
        {
            return "riptide/addReport";
        }
        try{
            if (extras != null){
                String extrasAsString = String.join(", ", extras);
                incident.setAdditionalOffenses(extrasAsString);
            }

            String dateCreated = LocalDate.now().toString();
            incident.setDateCreated(dateCreated);

            //repository statement to insert new incident to database
            ir.save(incident);
            model.addAttribute("incidents", loadIncidents());

            return "riptide/listIncidents";
        } catch(Exception e){
            return "riptide/addReport";
        }
    }
    
    /**
     * This controller displays the information of the selected incident
     * @author Elkeno Jones
     * Date 20161114
     * 
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping("/incidentShow")
    public String showIncident(Model model, HttpServletRequest request){
        //find the incident
        int incidentNum = Integer.parseInt(request.getParameter("id"));
        Incident theIncident = (Incident) ir.findOne(incidentNum);
        System.out.println("**********\n"+ incidentNum);
        // Add the incident to the session
        // Need to use this for adding the people to the correct incident
        request.getSession().setAttribute("incidentID", incidentNum);
        
        ArrayList<Codevalue> codeValues = getIncidentTypes();
        ArrayList<Codevalue> extraOffenses = getAdditionalOffencts();
        
        theIncident.setOffenses(theIncident.getAdditionalOffenses().split(", "));
        model.addAttribute("incident", theIncident);
        model.addAttribute("codeValues", codeValues);
        model.addAttribute("extras", extraOffenses);
        model.addAttribute("incidentID", theIncident.getIncidentNum());
        
        request.getSession().setAttribute("incidentID", incidentNum);
        
//        HttpSession hs = request.getSession();
//        
//        hs.setAttribute("id", incidentNum);
        
        return "riptide/editIncident";
    }
    
    /**
     * Controller that will save the incident to the database
     * @author Elkeno Jones_ejones109029
     * 
     * @param model
     * @param incident
     * @param extras
     * @return 
     */
    @RequestMapping("/saveIncident")
    public String saveEdit(Model model, @ModelAttribute("incident") Incident incident, @RequestParam(value="extras") String[] extras, HttpServletRequest request){
        System.out.println("**********************\n" + request.getSession().getAttribute("id"));
        incident.setIncidentNum((int)request.getSession().getAttribute("id"));
        
        if (extras != null){
            String extrasAsString = String.join(", ", extras);
            incident.setAdditionalOffenses(extrasAsString);
        }
        
        //repository statement to insert new incident to database
        saveIncidents(incident);
        model.addAttribute("incidents", loadIncidents());
        
        return "riptide/listIncidents";
    }
    
    // Reuseable method for calling the arraylist of incidents
    // @author Elkeno Jones_ejones109029
    public ArrayList<Incident> loadIncidents(){
        return (ArrayList) ir.findAll();
    }
    
    // Reuseable method for calling the save incident
    // @author Elkeno Jones_ejones109029
    public void saveIncidents(Incident incident){
        ir.save(incident);
    }
    // find incidentTypes for the dropdown list
    public ArrayList<Codevalue> getIncidentTypes(){
        return (ArrayList) codeRepo.findByCodeTypeId(2);
    }
    // find addition offenses for the checkboxes
    public ArrayList<Codevalue> getAdditionalOffencts(){
        return (ArrayList) codeRepo.findByCodeTypeId(3);
    }
}
