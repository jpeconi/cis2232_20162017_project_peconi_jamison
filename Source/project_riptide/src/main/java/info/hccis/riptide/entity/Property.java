/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.riptide.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author Elkeno
 */
@Entity
@Table(name = "property")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Property.findAll", query = "SELECT p FROM Property p")})
public class Property implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "propertyId")
    private Integer propertyId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "incidentNum")
    private int incidentNum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private float quantity;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 10)
    @Column(name = "measurement")
    private String measurement;
    @Size(max = 30)
    @Column(name = "seizedBy")
    private String seizedBy;
    @Size(max = 12)
    @Column(name = "badgeNum")
    private String badgeNum;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateSeized")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateSeized;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "timeSeized")
    private String timeSeized;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "storageLocation")
    private String storageLocation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "entryDate")
    @Temporal(TemporalType.DATE)
     @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date entryDate;
    @Size(max = 30)
    @Column(name = "owner")
    private String owner;

    public Property() {
    }

    public Property(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public Property(Integer propertyId, int incidentNum, float quantity, String description, Date dateSeized, String timeSeized, String storageLocation, Date entryDate) {
        this.propertyId = propertyId;
        this.incidentNum = incidentNum;
        this.quantity = quantity;
        this.description = description;
        this.dateSeized = dateSeized;
        this.timeSeized = timeSeized;
        this.storageLocation = storageLocation;
        this.entryDate = entryDate;
    }

    public Integer getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(Integer propertyId) {
        this.propertyId = propertyId;
    }

    public int getIncidentNum() {
        return incidentNum;
    }

    public void setIncidentNum(int incidentNum) {
        this.incidentNum = incidentNum;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getSeizedBy() {
        return seizedBy;
    }

    public void setSeizedBy(String seizedBy) {
        this.seizedBy = seizedBy;
    }

    public String getBadgeNum() {
        return badgeNum;
    }

    public void setBadgeNum(String badgeNum) {
        this.badgeNum = badgeNum;
    }

    public Date getDateSeized() {
        return dateSeized;
    }

    public void setDateSeized(Date dateSeized) {
        this.dateSeized = dateSeized;
    }

    public String getTimeSeized() {
        return timeSeized;
    }

    public void setTimeSeized(String timeSeized) {
        this.timeSeized = timeSeized;
    }

    public String getStorageLocation() {
        return storageLocation;
    }

    public void setStorageLocation(String storageLocation) {
        this.storageLocation = storageLocation;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (propertyId != null ? propertyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Property)) {
            return false;
        }
        Property other = (Property) object;
        if ((this.propertyId == null && other.propertyId != null) || (this.propertyId != null && !this.propertyId.equals(other.propertyId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "info.hccis.riptide.entity.Property[ propertyId=" + propertyId + " ]";
    }
    
}
