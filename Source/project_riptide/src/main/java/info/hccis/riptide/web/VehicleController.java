package info.hccis.riptide.web;

import info.hccis.riptide.data.springdatajpa.CodeValueRepository;
import info.hccis.riptide.data.springdatajpa.UserAccessRepository;
import info.hccis.riptide.data.springdatajpa.VehicleRepository;
import info.hccis.riptide.entity.Vehicle;
import info.hccis.riptide.useraccess.entity.Codevalue;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author pfouchere
 * @since November 3, 2016
 * @purpose Controller for Vehicle page
 * Does various things such as add, edit, delete.
 */
@Controller
public class VehicleController {

    private final UserAccessRepository userAccessRepository;
    private final VehicleRepository vehicleRepository;
    private final CodeValueRepository codeValueRepo;

    @Autowired
    public VehicleController(UserAccessRepository userAccessRepository, VehicleRepository vehicleRepository, CodeValueRepository codeValueRepo) {
        this.userAccessRepository = userAccessRepository;
        this.vehicleRepository = vehicleRepository;
        this.codeValueRepo = codeValueRepo;
    }

    @RequestMapping("/vehicle")
    public String showVehiclePage(Model model, HttpServletRequest request) {
        int incidentNum = (int) request.getSession().getAttribute("incidentID");

        ArrayList<Vehicle> vehicles = null;
        ArrayList<Codevalue> colors = null;
        try {
            vehicles = (ArrayList<Vehicle>) vehicleRepository.findByIncidentNum(incidentNum);
            colors = (ArrayList<Codevalue>) codeValueRepo.findByCodeTypeId(4);
            request.getSession().setAttribute("colors", colors);
            //This is a small change
            System.out.println("Small change");
        } catch (Exception e) {
            System.out.println(e);
        }
        // Grab the incident ID that we are currently working on
        // This is gonna be used to add the people to the right incident
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("vehicles", vehicles);
        model.addAttribute("colors", colors);
        Vehicle temp = new Vehicle();
        model.addAttribute("vehicle", temp);
        return "riptide/vehicle";
    }

    @RequestMapping(value = "/vehicle/getDetails", headers = "Accept=*/*", method = RequestMethod.GET)
    public @ResponseBody
    Vehicle getDetails(@RequestParam("vin") String vin, Model model) {
        List<Vehicle> result = vehicleRepository.findByVinNum(vin);
        return result.get(0);
    }

    @RequestMapping("/vehicle/add")
    public String addVehicle(Model model, @ModelAttribute("vehicle") @Valid Vehicle vehicle, BindingResult bindingResult, HttpServletRequest request) {
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        ArrayList<Codevalue> colors = (ArrayList<Codevalue>) request.getSession().getAttribute("colors");

        // Check to see if the form data has errors
        if (bindingResult.hasErrors()) {
            // Return to the form page if so          
            ArrayList<Vehicle> vehicles = (ArrayList<Vehicle>) vehicleRepository.findByIncidentNum(incidentNum);
            // Grab the incident ID that we are currently working on
            // This is gonna be used to add the people to the right incident
            model.addAttribute("colors", colors);
            model.addAttribute("incidentID", incidentNum);
            model.addAttribute("vehicles", vehicles);
            return "riptide/vehicle";
        }

        vehicleRepository.save(vehicle);
        ArrayList<Vehicle> vehicles = (ArrayList<Vehicle>) vehicleRepository.findByIncidentNum(incidentNum);
        // Grab the incident ID that we are currently working on
        // This is gonna be used to add the people to the right incident
        model.addAttribute("colors", colors);
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("vehicles", vehicles);
        Vehicle temp = new Vehicle();
        model.addAttribute("vehicle", temp);
        return "riptide/vehicle";

    }

    @RequestMapping("/vehicle/delete")
    public String deleteVehicle(Model model, HttpServletRequest request) {

        //grab incident ID
        int incidentNum = (int) request.getSession().getAttribute("incidentID");
        //Grab list of vehicles for the incident
        ArrayList<Vehicle> vehicles = (ArrayList<Vehicle>) vehicleRepository.findByIncidentNum(incidentNum);

        //conditional statement to re-route if nothing is selected.
        if (request.getParameter("id") == null || request.getParameter("id").isEmpty()) {
            model.addAttribute("incidentID", incidentNum);
            model.addAttribute("vehicles", vehicles);
            Vehicle temp = new Vehicle();
            model.addAttribute("vehicle", temp);
            return "riptide/vehicle";
        }
        //delete query
        vehicleRepository.deleteByVinNum(request.getParameter("id"));
        
        //regenerates arraylist for re-display after change.
        vehicles = (ArrayList<Vehicle>) vehicleRepository.findByIncidentNum(incidentNum);
        //Add back to the model
        model.addAttribute("incidentID", incidentNum);
        model.addAttribute("vehicles", vehicles);
        Vehicle temp = new Vehicle();
        model.addAttribute("vehicle", temp);
        return "riptide/vehicle";
    }

}
