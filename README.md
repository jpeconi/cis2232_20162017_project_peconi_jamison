# README #
Welcome to the cis 2232 2016/2017 final project. This is an incident tracker built for the Summerside Police Academy. This application is built in Java using the Spring Web Framework. This application is for the cadets to enter incident data into a common database as oppose to manually entering the data in paper format. This application also contains links to all the files required to be filled out for the court. They can be downloaded through the documents link inside the incident. My portion was the person portion of the project. 


### How do I get set up? ###

* To setup up the incident tracker you must clone the project to your local machine. After the project is cloned, you must first run the database script. The database script is called createdatabase.sql and is located in the other sources folder, in the db.mysql package. After running the create database script simply launch the program through netbeans and fill out the forms.

* The default username: admin password: test

### Contribution guidelines ###

* If you would like to contribute to this project, submit a pull request and it will be reviewed before being merged into the main branch.

### Who do I talk to? ###

* Jamison Peconi
* CIS 2232 - Charlottetown PEI - Holland College